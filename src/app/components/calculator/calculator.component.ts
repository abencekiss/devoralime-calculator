import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
})
export class CalculatorComponent implements OnInit {
  private lastInputType: string = '';
  public lastNumber: string = '0';
  private lastOperator: string = '';

  private isFirstAction: boolean = true;
  private isFractionNumber: boolean = false;

  public resultHistory: Array<string> = [];
  public storedResult: string = '';

  public result: string = '';

  private numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'];

  private operators = ['+', '-', '*', '/', '='];

  private actions = ['CE', 'C', 'X'];

  constructor() {}

  resetLastNumber() {
    this.lastNumber = '0';
    this.isFirstAction = true;
    this.isFractionNumber = false;
  }

  initCalculator() {
    this.lastNumber = '0';
    this.lastInputType = '';
    this.lastOperator = '';
    this.isFirstAction = true;
    this.isFractionNumber = false;
  }

  resetCalculator() {
    this.initCalculator();
    this.resultHistory = [];
    this.storedResult = '';
    this.result = '';
  }

  calculateResult() {
    this.initCalculator();
    this.resultHistory = [];
    this.result = parseFloat(this.storedResult)
      .toLocaleString('hu-HU')
      .split(',')
      .join('.');
    this.storedResult = '';
  }

  calculate(a: number, b: number, operator: string): number {
    let calculation = 0;

    if (operator === '+') {
      calculation = a + b;
    } else if (operator === '-') {
      calculation = a - b;
    } else if (operator === '*') {
      calculation = a * b;
    } else if (operator === '/') {
      calculation = a / b;
    }

    return calculation;
  }

  pressedButton(button: string) {
    let pressedButton: string = '';
    let pressedButtonType: string = '';

    // Check pressed button and set it's type
    pressedButton = button;

    this.numbers.every((number) => {
      if (number === button) {
        pressedButtonType = 'Number';
        return false;
      } else if (pressedButtonType !== '') {
        return false;
      }

      return true;
    });

    this.operators.every((operator) => {
      if (operator === button) {
        pressedButtonType = 'Operator';
        return false;
      } else if (pressedButtonType !== '') {
        return false;
      }

      return true;
    });

    this.actions.every((action) => {
      if (action === button) {
        pressedButtonType = 'Action';
        return false;
      } else if (pressedButtonType !== '') {
        return false;
      }

      return true;
    });
    // ===

    // Reset everything if previous result shown
    if (this.result !== '') {
      this.resetCalculator();
    }
    // ===

    // Return if operator (excluding subtraction) or action button pressed first
    if (
      this.lastNumber === '0' &&
      ((pressedButtonType === 'Operator' &&
        pressedButton !== '-' &&
        pressedButton !== '.') ||
        pressedButtonType === 'Action')
    ) {
      return;
    }
    // ===

    // Actions
    if (pressedButtonType === 'Action') {
      if (pressedButton === 'CE') {
        this.lastNumber = '0';
      } else if (pressedButton === 'C') {
        this.resetCalculator();
        console.log('RESET CALCULATION');
      } else if (pressedButton === 'X') {
        if (this.lastNumber !== '0') {
          if (this.lastNumber.length !== 1) {
            this.lastNumber = this.lastNumber.slice(0, -1);
          } else {
            this.lastNumber = '0';
          }
        }
      }
      return;
    }
    // ===

    // Numbers
    if (pressedButtonType === 'Number') {
      // Reset calculator if first action initiated
      if (this.isFirstAction === true) {
        this.resetLastNumber();
      }
      // ===

      this.isFirstAction = false;

      // If fraction button pressed twice
      if (this.isFractionNumber && pressedButton === '.') {
        return;
      }
      // ===

      // If fraction button presed set isFractionNumber true to avoid multiple button press
      if (pressedButton === '.') {
        this.isFractionNumber = true;
      }
      // ===

      if (this.lastNumber === '0') {
        if (pressedButton === '.') {
          // If starts with zero and fraction button pressed add fraction after zero
          this.lastNumber = '0' + pressedButton;
        } else {
          // Change initial zero number to the pressed number
          this.lastNumber = pressedButton;
        }
      } else {
        // Add pressed number after the stored number
        this.lastNumber += pressedButton;
      }
    }
    // ===

    // Operators
    if (pressedButtonType === 'Operator') {
      // Return if operator button pressed twice and change last operator
      if (
        this.lastInputType === 'Operator' &&
        pressedButtonType === 'Operator' && pressedButton !== '='
      ) {
        this.lastOperator = pressedButton;
        this.resultHistory[this.resultHistory.length - 1] = pressedButton;
        return;
      }

      if (pressedButton === '=') {
        this.storedResult = this.calculate(
          parseFloat(this.storedResult),
          parseFloat(this.lastNumber),
          this.lastOperator,
        ).toString();
        this.calculateResult();
      } else {
        // Set isFirsAction to true after every operation to reset number screen after calculation
        this.isFirstAction = true;

        if (this.resultHistory.length === 0) {
          // If resultHistory is empty store the first number and operator and set storedResult value to the first number
          this.resultHistory.push(this.lastNumber, pressedButton);
          this.storedResult = this.lastNumber;
          this.lastInputType = pressedButtonType;
          this.lastOperator = pressedButton;
          return;
        } else {
          // Calculate new stored result value then set lastNumber to the calculated value
          this.resultHistory.push(this.lastNumber, pressedButton);
          this.storedResult = this.calculate(
            parseFloat(this.storedResult),
            parseFloat(this.lastNumber),
            this.lastOperator,
          ).toString();
          this.lastNumber = this.storedResult;
        }
      }

      // Save current operator for future use
      this.lastOperator = pressedButton;
    }
    // ===

    // Save always the last button type for safety checks
    this.lastInputType = pressedButtonType;
  }

  ngOnInit(): void {}
}
